const pesanan = require("../model/pesanan");
const customer = require("../model/customer");
const room = require("../model/room");

exports.postPesanan = async (req,res,next)=>{
    const dataPesanan = new pesanan(req.body);
    try {
        // Scope Variabel
        await req,dataPesanan;
        // Mencari nama di dalam database customer
        customer.find({nama:req.body.customer.nama},function(error,hasil){
            if(!hasil.length){
                res.status(500).json({
                    success:false,
                    data: "NAMA TIDAK TERDAFTAR !!"
                })
            } else {
                // Mencari room dalam database room
                room.find({nokamar:req.body.kamar.nokamar},function(error,hasil){ 
                    if(!hasil.length){
                        res.status(500).json({
                            success:false,
                            data: "ROOM TIDAK TERDAFTAR !!"
                        })
                    } else {
                        dataPesanan.save();
                        res.status(200).json({
                            success:true,
                            data:dataPesanan
                        })
                    }
                })
            }
        })
    } catch (error) {
        res.status(500).json({
            success:false,
            data:error
        })
    }
}

exports.getDataPesanan = async (req,res,next)=>{
    const dataPesanan = await pesanan.find();
    res.status(200).json({
        success:true,
        data:dataPesanan
    })
}

exports.editPesanan =  async(req,res,next)=>{
    const id = req.params.id
    await pesanan.updateOne({_id:id},req.body);
    res.status(200).json({
        success:true,
        data:"update success"
    })
}

exports.hapusPesanan =  async(req,res,next)=>{
    const id = req.params.id
    const data = await pesanan.deleteOne({_id:id});
    res.status(200).json({
        success:true,
        data:"delete success"
    })
}