const room = require("../model/room");

exports.postRoom = async (req,res,next)=>{
    const dataRoom = new room(req.body);
    try {
        await dataRoom.save();
        res.status(200).json({
            success:true,
            data:dataRoom
        })
    } catch (error) {
        res.status(500).json({
            success:false,
            data:error
        })
    }
}

exports.editRoom =  async(req,res,next)=>{
    const id = req.params.id
    await room.updateOne({_id:id},req.body);
    res.status(200).json({
        success:true,
        data:"update success"
    })
}

exports.hapusRoom =  async(req,res,next)=>{
    const id = req.params.id
    const data = await room.deleteOne({_id:id});
    res.status(200).json({
        success:true,
        data:"delete success"
    })
}