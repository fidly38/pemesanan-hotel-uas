const adm = require("../model/admin");

exports.postAdmin = async (req,res,next)=>{
    const dataAdmin = new adm(req.body);
    try {
        await dataAdmin.save();
        res.status(200).json({
            success:true,
            data:dataAdmin
        })
    } catch (error) {
        res.status(500).json({
            success:false,
            data:error
        })
    }
}

exports.editAdmin =  async(req,res,next)=>{
    const id = req.params.id
    await adm.updateOne({_id:id},req.body);
    res.status(200).json({
        success:true,
        data:"update success"
    })
}

exports.hapusAdmin =  async(req,res,next)=>{
    const id = req.params.id
    const data = await adm.deleteOne({_id:id});
    res.status(200).json({
        success:true,
        data:"delete success"
    })
}