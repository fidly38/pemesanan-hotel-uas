const customer = require("../model/customer");
const room = require("../model/room");

exports.postCustomer = async (req,res,next)=>{
    const dataCustomer = new customer(req.body);
    try {
        await dataCustomer;
        room.find({nokamar:req.body.kamar.nokamar},function(error,hasil){ 
            if(!hasil.length){
                res.status(500).json({
                    success:false,
                    data: "KAMAR TIDAK TERSEDIA !!"
                })
            } else {
                dataCustomer.save();
                res.status(200).json({
                success:true,
                data:dataCustomer
                })
            }
        })
    } catch (error) {
        res.status(500).json({
            success:false,
            data:error
        })
    }
}

exports.editCustomer =  async(req,res,next)=>{
    const id = req.params.id
    await customer.updateOne({_id:id},req.body);
    res.status(200).json({
        success:true,
        data:"update success"
    })
}

exports.hapusCustomer =  async(req,res,next)=>{
    const id = req.params.id
    const data = await customer.deleteOne({_id:id});
    res.status(200).json({
        success:true,
        data:"delete success"
    })
}

exports.getDataCustomer = async (req,res,next)=>{
    const dataCustomer = await customer.find();
    res.status(200).json({
        success:true,
        data:dataCustomer
    })
}