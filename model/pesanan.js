const db = require('mongoose');
const Schema = db.Schema;
const pesanan = new Schema({
    checkin : {
        type : String
    },
    checkout : {
        type : String
    },
    customer:{},
    kamar:{},
    admin:{}
});

module.exports = db.model('pesanan', pesanan);