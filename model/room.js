const db = require('mongoose');
const Schema = db.Schema;
const room = new Schema({
    nokamar : {
        type : String
    },
    tipe : {
        type : String
    },
    harga : {
        type : String
    },
    kapasitas : {
        type : String
    }
});
module.exports = db.model('kamar', room);