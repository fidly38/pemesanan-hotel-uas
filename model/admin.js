const db = require('mongoose');
const Schema = db.Schema;
const adm = new Schema({
    idadmin : {
        type : String
    },
    nama : {
        type : String
    },
    notlp : {
        type : Number
    }
});
module.exports = db.model('admin', adm);