const db = require('mongoose');
const Schema = db.Schema;
const customer = new Schema({
    nama : {
        type : String
    },
    alamat : {
        type : String
    },
    usia : {
        type : Number
    },
    nohp : {
        type : Number
    }, 
    kamar:{}
});
module.exports = db.model('customer', customer);