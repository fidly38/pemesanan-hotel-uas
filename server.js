const uasexpress = require('express');
const server = uasexpress();
const port = 2022;

const parser = require('body-parser');
server.use(parser.json())

const db = require('mongoose')
const conn = "mongodb://localhost:27017/hotel"
db.connect(conn);
const connection = db.connection;
connection.once("open",function(){
    console.log("Database Terkoneksi")
})

const appRouter = require("./router/appRouter.js")

server.use("/hotel",appRouter)

server.listen(port,function(){
    console.log("Server "+port);
})