const server = require("express");
const { body } = require("express-validator")
const router = server.Router();
const customerController = require("../controller/customerController");
const roomController = require("../controller/roomController");
const admController = require("../controller/adminController");
const pesananController = require("../controller/pesananController");

//Route Customer
router.post("/postCustomer",customerController.postCustomer);
router.post("/editCustomer/:id",customerController.editCustomer);
router.delete("/hapusCustomer/:id",customerController.hapusCustomer);
router.get("/getCustomer",customerController.getDataCustomer);


//Route Room
router.post("/postRoom",roomController.postRoom);
router.post("/editRoom/:id",roomController.editRoom);
router.delete("/hapusRoom/:id",roomController.hapusRoom);

//Route Admin
router.post("/postAdmin",admController.postAdmin);
router.post("/editAdmin/:id",admController.editAdmin);
router.delete("/hapusAdmin/:id",admController.hapusAdmin);

//Route Pesanan
router.post("/postPesanan",pesananController.postPesanan);
router.get("/getPesanan",pesananController.getDataPesanan);
router.delete("/hapusPesanan/:id",pesananController.hapusPesanan);




module.exports = router;